package TareasBD;

/**
 *
 * @author dani
 */
import java.util.ArrayList;

public class Tarea {
	private static int contador=0;
	private String nombre;
	private int id;
	private ArrayList<Detalle>detalles;
	
	public Tarea(String nombre) {
		this.nombre=nombre;
		this.id=contador;
		contador++;
		detalles=new ArrayList();
	}
	
	public void addDetalle(Detalle det) {
		detalles.add(det);
	}
	public void addDetalles(ArrayList <Detalle> detalles) {
		this.detalles=detalles;
	}
	

	public static int getContador() {
		return contador;
	}

	public static void setContador(int contador) {
		Tarea.contador = contador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Detalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(ArrayList<Detalle> detalles) {
		this.detalles = detalles;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Nombre de tarea: "+this.nombre+" \tId :"+id;
	}
	
	
	
	
	
	
	

}
